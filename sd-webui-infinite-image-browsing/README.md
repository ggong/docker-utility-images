expected usage is to mount your image directories at /outputs

pass in PORT and EXTRA_PATHS if desired

defaults:
```
PORT=8080
EXTRA_PATHS=/outputs
```

example:
```
docker run --env PORT=8081 --env EXTRA_PATHS=/outputs -v /outputs_location:/outputs -d -p 8081:8081 dynafire/sd-webui-infinite-image-browsing
docker run -v /outputs_location:/outputs -it -p 8081:8080 dynafire/sd-webui-infinite-image-browsing
docker run -v /outputs_location:/outputs:ro -d -p 8080:8080 dynafire/sd-webui-infinite-image-browsing
docker run --env EXTRA_OPTIONS="--allow_cors --update_image_index" -v /outputs_location:/outputs:ro -d -p 8080:8080 dynafire/sd-webui-infinite-image-browsing
```
