# run_server.py

import os
from gevent import monkey
monkey.patch_all()  # Enable gevent monkey-patching for async behavior

from gevent.pool import Pool
from gevent.pywsgi import WSGIServer
from api_v2 import app  # Import the app from api_v2.py

# Create a Pool to limit concurrent connections
# inference is GPU limited to 1 at a time so we only want a little concurrency
# so that network transmission and transcoding are done async
pool = Pool(2)  # Limit to 2 concurrent connections

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    http_server = WSGIServer(('0.0.0.0', port), app, spawn=pool)
    http_server.serve_forever()
